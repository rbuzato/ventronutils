package vdocumentutils;

import org.junit.Assert;


import br.com.Ventron.Utils.VCnpjUtils;
import org.junit.Test;

class VCnpjUtilsTest {

	@Test
	public void testCNPJvalido() {
		Assert.assertTrue(VCnpjUtils.isCNPJ("64277736000194"));
	}
	
	@Test
	public void testCNPJinvalido() {
		Assert.assertFalse(VCnpjUtils.isCNPJ("64277736000199"));
	}

}
