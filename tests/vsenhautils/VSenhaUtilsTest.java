package vsenhautils;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;

import br.com.Ventron.Utils.VSenhaUtils;

import org.junit.Assert;
import org.junit.Test;

public class VSenhaUtilsTest {

	@Test
	public void testSenha256Valido() throws NoSuchAlgorithmException {
		String senha = "senha";
		String senhaCriptografada = VSenhaUtils.criptografarSenhaSha256(senha);

		Assert.assertEquals(senhaCriptografada, VSenhaUtils.criptografarSenhaSha256(senha));
	}

	@Test
	public void testSenha256Invalido() throws NoSuchAlgorithmException {
		String senha = "senha";
		String senhaCriptografada = VSenhaUtils.criptografarSenhaSha256(senha);

		Assert.assertNotEquals(senhaCriptografada, VSenhaUtils.criptografarSenhaSha256("senha "));
	}

	@Test
	public void testSenha512Valido() throws NoSuchAlgorithmException {
		String senha = "senha";
		String senhaCriptografada = VSenhaUtils.criptografarSenhaSha512(senha);

		Assert.assertEquals(senhaCriptografada, VSenhaUtils.criptografarSenhaSha512(senha));
	}

	@Test
	public void testSenha512Invalido() throws NoSuchAlgorithmException {
		String senha = "senha";
		String senhaCriptografada = VSenhaUtils.criptografarSenhaSha512(senha);

		Assert.assertNotEquals(senhaCriptografada, VSenhaUtils.criptografarSenhaSha512("senha "));
	}

}
