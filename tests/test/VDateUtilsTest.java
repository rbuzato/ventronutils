package test;

import br.com.Ventron.Utils.VDateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;

public class VDateUtilsTest {

    private Date d1;
    private Date d2;

    @Before
    public void setUp() throws Exception {
        Calendar c = Calendar.getInstance();
        // Data: 10/09/2018
        c.set(2018, 8, 10);
        d1 = c.getTime();
        // Data: 25/12/2020
        c.set(2020, 11, 25);
        d2 = c.getTime();
    }

    @Test
    public void testarDiaNovaData(){
        Calendar c = Calendar.getInstance();
        Assert.assertTrue(c.getTime().getDay() == VDateUtils.newDate().getDay());
    }

    @Test
    public void testarMesNovaData(){
        Calendar c = Calendar.getInstance();
        Assert.assertTrue(c.getTime().getMonth() == VDateUtils.newDate().getMonth());
    }

    @Test
    public void testarAnoNovaData(){
        Calendar c = Calendar.getInstance();
        Assert.assertTrue(c.getTime().getYear() == VDateUtils.newDate().getYear());
    }

    @Test
    public void testarMinutoNovaData(){
        Calendar c = Calendar.getInstance();
        Assert.assertTrue(c.getTime().getMinutes() == VDateUtils.newDate().getMinutes());
    }

    @Test
    public void testarHoraNovaData(){
        Calendar c = Calendar.getInstance();
        Assert.assertTrue(c.getTime().getHours() == VDateUtils.newDate().getHours());
    }

    @Test
    public void testarNovaDataErrada(){
        Assert.assertFalse(d1.equals(VDateUtils.newDate()));
    }

    @Test
    public void testarDiferencaEntreDatasDiaPassou(){
        Assert.assertEquals(new Long(838), VDateUtils.getDaysfromDates(d1,d2));
    }

    @Test
    public void testarDiferencaEntreDatasDiaFalhou(){
        Assert.assertNotEquals(new Long(400), VDateUtils.getDaysfromDates(d1,d2));
    }

    @Test(expected = Exception.class)
    public void testarExceptionEntreDatasDia(){
        d1 = null;
        VDateUtils.getDaysfromDates(d1,d2);
    }

    @Test
    public void testarDiferencaEntreDatasMesPassou(){
        Assert.assertEquals(new Long(27), VDateUtils.getMonthsfromDates(d1,d2));
    }

    @Test
    public void testarDiferencaEntreDatasMesFalhou(){
        Assert.assertNotEquals(new Long(28), VDateUtils.getMonthsfromDates(d1,d2));
    }

    @Test(expected = Exception.class)
    public void testarExceptionEntreDatasMes(){
        d1 = null;
        VDateUtils.getMonthsfromDates(d1,d2);
    }

    @Test
    public void testarDiferencaEntreDatasAnoPassou(){
        Assert.assertEquals(new Long(2), VDateUtils.getYearsfromDates(d1,d2));
    }

    @Test
    public void testarDiferencaEntreDatasAnoFalhou(){
        Assert.assertNotEquals(new Long(10), VDateUtils.getYearsfromDates(d1,d2));
    }

    @Test(expected = Exception.class)
    public void testarExceptionEntreDatasAno(){
        d1 = null;
        VDateUtils.getYearsfromDates(d1,d2);
    }


}


