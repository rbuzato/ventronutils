package vdateutils;

import br.com.Ventron.Utils.VDateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Calendar;

public class VDateUtilsAddTimeToDateTest {

    @Test
    public void addDaysToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addDaysToDate(null, 5));
    }

    @Test
    public void addDaysToDateDiasNullTest() {
        Assert.assertNull(VDateUtils.addDaysToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addMonthsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMonthsToDate(null, 5));
    }

    @Test
    public void addMonthsToDateMesesNullTest() {
        Assert.assertNull(VDateUtils.addMonthsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addYearsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addYearsToDate(null, 5));
    }

    @Test
    public void addYearsToDateAnosNullTest() {
        Assert.assertNull(VDateUtils.addYearsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addHoursToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addHoursToDate(null, 5));
    }

    @Test
    public void addHoursToDateHorasNullTest() {
        Assert.assertNull(VDateUtils.addHoursToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addMinutesToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMinutesToDate(null, 5));
    }

    @Test
    public void addMinutesToDateMinutosNullTest() {
        Assert.assertNull(VDateUtils.addMinutesToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addSecondsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addSecondsToDate(null, 5));
    }

    @Test
    public void addSecondsToDateSegundosNullTest() {
        Assert.assertNull(VDateUtils.addSecondsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void addMillisecondsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMillisecondsToDate(null, 5));
    }

    @Test
    public void addMillisecondsToDateMillisegundosNullTest() {
        Assert.assertNull(VDateUtils.addMillisecondsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subDaysToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addDaysToDate(null, -5));
    }

    @Test
    public void subDaysToDateDiasNullTest() {
        Assert.assertNull(VDateUtils.addDaysToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subMonthsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMonthsToDate(null, -5));
    }

    @Test
    public void subMonthsToDateMesesNullTest() {
        Assert.assertNull(VDateUtils.addMonthsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subYearsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addYearsToDate(null, -5));
    }

    @Test
    public void subYearsToDateAnosNullTest() {
        Assert.assertNull(VDateUtils.addYearsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subHoursToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addHoursToDate(null, -5));
    }

    @Test
    public void subHoursToDateHorasNullTest() {
        Assert.assertNull(VDateUtils.addHoursToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subMinutesToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMinutesToDate(null, -5));
    }

    @Test
    public void subMinutesToDateMinutosNullTest() {
        Assert.assertNull(VDateUtils.addMinutesToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subSecondsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addSecondsToDate(null, -5));
    }

    @Test
    public void subSecondsToDateSegundosNullTest() {
        Assert.assertNull(VDateUtils.addSecondsToDate(VDateUtils.newDate(), null));
    }

    @Test
    public void subMillisecondsToDateDataNullTest() {
        Assert.assertNull(VDateUtils.addMillisecondsToDate(null, -5));
    }

    @Test
    public void subMillisecondsToDateMillisegundosNullTest() {
        Assert.assertNull(VDateUtils.addMillisecondsToDate(VDateUtils.newDate(), null));
    }

}
