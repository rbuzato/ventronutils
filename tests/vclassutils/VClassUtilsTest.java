package vclassutils;

import br.com.Ventron.Utils.VClassUtils;
import br.com.Ventron.Utils.VDateUtils;
import mock.ModeloObjeto;
import org.junit.Assert;
import org.junit.Test;

public class VClassUtilsTest {

    @Test
    public void containsNullFieldTest() {
        ModeloObjeto objeto = new ModeloObjeto();
        objeto.setAtributoInteger(12);
        objeto.setAtributoDouble(2.5);
        objeto.setAtributoString("teste");
        objeto.setAtributoDate(null);
        objeto.setAtributoBoolean(false);

        Assert.assertTrue(VClassUtils.containsNullField(objeto));
    }

    @Test
    public void notContainsNullFieldTest() {
        ModeloObjeto objeto = new ModeloObjeto();
        objeto.setAtributoInteger(12);
        objeto.setAtributoDouble(2.5);
        objeto.setAtributoString("teste");
        objeto.setAtributoDate(VDateUtils.newDate());
        objeto.setAtributoBoolean(false);

        Assert.assertFalse(VClassUtils.containsNullField(objeto));
    }
}
