package mock;

import java.util.Date;

public class ModeloObjeto {

    private Integer atributoInteger;
    private Double atributoDouble;
    private String atributoString;
    private Boolean atributoBoolean;
    private Date atributoDate;


    public ModeloObjeto(Integer atributoInteger, Double atributoDouble, String atributoString, boolean atributoBoolean, Date atributoDate) {
        this.atributoInteger = atributoInteger;
        this.atributoDouble = atributoDouble;
        this.atributoString = atributoString;
        this.atributoBoolean = atributoBoolean;
        this.atributoDate = atributoDate;
    }

    public ModeloObjeto(){
    }

    public Integer getAtributoInteger() {
        return atributoInteger;
    }

    public void setAtributoInteger(Integer atributoInteger) {
        this.atributoInteger = atributoInteger;
    }

    public Double getAtributoDouble() {
        return atributoDouble;
    }

    public void setAtributoDouble(Double atributoDouble) {
        this.atributoDouble = atributoDouble;
    }

    public String getAtributoString() {
        return atributoString;
    }

    public void setAtributoString(String atributoString) {
        this.atributoString = atributoString;
    }

    public boolean isAtributoBoolean() {
        return atributoBoolean;
    }

    public void setAtributoBoolean(boolean atributoBoolean) {
        this.atributoBoolean = atributoBoolean;
    }

    public Date getAtributoDate() {
        return atributoDate;
    }

    public void setAtributoDate(Date atributoDate) {
        this.atributoDate = atributoDate;
    }
}
