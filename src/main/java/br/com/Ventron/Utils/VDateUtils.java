package br.com.Ventron.Utils;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static java.text.DateFormat.FULL;

/**
 * Classe de utilitários, usada para manipulação de datas
 */
public class VDateUtils {


    /**
     * Método usado para instanciar uma data atual.
     * @return Data Atual
     */
    public static Date newDate(){
        Calendar c = Calendar.getInstance();
        return c.getTime();
    }

    /**
     * Metodo usado para pegar a diferenca entre duas datas em dias.
     * @param d1 data menor
     * @param d2 data posterior
     * @return Quantidade de dias entre as datas
     */
    public static Long getDaysfromDates(Date d1, Date d2){
        try {
            return ChronoUnit.DAYS.between(LocalDate.of(d1.getYear(), d1.getMonth(), d1.getDate()), LocalDate.of(d2.getYear(), d2.getMonth(), d2.getDate()));
        }catch (Exception ex){
            throw ex;
        }

    }

    /**
     * Metodo usado para pegar a diferenca entre duas datas em meses.
     * @param d1 data menor
     * @param d2 data posterior
     * @return Quantidade de meses entre as datas, arrendondando pra baixo.
     */
    public static Long getMonthsfromDates(Date d1, Date d2){
        try {
            return ChronoUnit.MONTHS.between(LocalDate.of(d1.getYear(), d1.getMonth(), d1.getDate()), LocalDate.of(d2.getYear(), d2.getMonth(), d2.getDate()));
        }catch (Exception ex){
            throw ex;
        }
    }


    /**
     * Metodo usado para pegar a diferenca entre duas datas em anos.
     * @param d1 data menor
     * @param d2 data posterior
     * @return Quantidade de anos entre as datas, arrendondando pra baixo.
     */
    public static Long getYearsfromDates(Date d1, Date d2){
        try {
            return ChronoUnit.YEARS.between(LocalDate.of(d1.getYear(), d1.getMonth(), d1.getDate()), LocalDate.of(d2.getYear(), d2.getMonth(), d2.getDate()));
        }catch (Exception ex){
            throw ex;
        }
    }



    /**
     * Método usado para converter uma String em um objeto Date.
     * @param data String no formato dd/MM/yyyy a ser formatada
     * @return date Date ou null caso receba uma String vazia ou nula
     * @throws Exception Caso a String esteja no formato errado
     */
    public static Date stringToDate(String data) throws ParseException {
        if(data == null){
            return null;
        }
        if(StringUtils.isEmpty(StringUtils.deleteWhitespace(data))) {
            try {
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date date = formatter.parse(data);
                return date;
            } catch (ParseException e) {
                throw e;
            }
        }
        return null;
    }

    /**
     * @param date
     * @return
     */
    public static String convertDateToBrPattern(Date date) {
        if(date == null)
            return "";
        Locale loc = new Locale("pt", "BR");
        DateFormat df = DateFormat.getDateInstance(FULL, loc);
        return df.format(date);
    }

    /**
     * Método usado para adicionar/subtrair dias em um objeto Date.
     * @param date Date, days Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou dias nulos;
     */
    public static Date addDaysToDate(Date date, Integer days) {
        if(date == null || days == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        date = calendar.getTime();

        return date;
    }


    /**
     * Método usado para adicionar/subtrair meses em um objeto Date.
     * @param date Date, months Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou meses nulos;
     */
    public static Date addMonthsToDate(Date date, Integer months) {
        if(date == null || months == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, months);
        date = calendar.getTime();

        return date;
    }

    /**
     * Método usado para adicionar/subtrair anos em um objeto Date.
     * @param date Date, years Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou anos nulos;
     */
    public static Date addYearsToDate(Date date, Integer years) {
        if(date == null || years == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        date = calendar.getTime();

        return date;
    }

    /**
     * Método usado para adicionar/subtrair horas em um objeto Date.
     * @param date Date, hours Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou horas nulos;
     */
    public static Date addHoursToDate(Date date, Integer hours) {
        if(date == null || hours == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        date = calendar.getTime();

        return date;
    }

    /**
     * Método usado para adicionar/subtrair minutos em um objeto Date.
     * @param date Date, minutes Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou minutos nulos;
     */
    public static Date addMinutesToDate(Date date, Integer minutes) {
        if(date == null || minutes == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        date = calendar.getTime();

        return date;
    }

    /**
     * Método usado para adicionar/subtrair segundos em um objeto Date.
     * @param date Date, seconds Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou segundos nulos;
     */
    public static Date addSecondsToDate(Date date, Integer seconds) {
        if(date == null || seconds == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, seconds);
        date = calendar.getTime();

        return date;
    }

    /**
     * Método usado para adicionar/subtrair millisegundos em um objeto Date.
     * @param date Date, milliseconds Integer pode assumir valor negativo para subtração
     * @return date Date ou null caso receba uma Data ou millisegundos nulos;
     */
    public static Date addMillisecondsToDate(Date date, Integer milliseconds) {
        if(date == null || milliseconds == null) {
            return null;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MILLISECOND, milliseconds);
        date = calendar.getTime();

        return date;
    }
}
