package br.com.Ventron.Utils;

import java.util.InputMismatchException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VCnpjUtils {

	public static boolean isCNPJ(String CNPJ) {

		if (!isValido(CNPJ)) {
			return (false);
		}

		char dig13, dig14;
		int sm, r;

		try {

			sm = calculationOfFirstVerifierDigit(CNPJ);

			r = sm % 11;
			dig13 = checkTheSumOfRest(r);

			sm = calculationOfSecondVerifierDigit(CNPJ);

			r = sm % 11;
			
			dig14 = checkTheSumOfRest(r);

			return caracterIsValidInPosition(CNPJ, dig13, dig14);

		} catch (InputMismatchException erro) {

			return (false);
		}
	}

	private static boolean isValido(String CNPJ) {
		String regex = "\\d{2}.?\\d{3}.?\\d{3}/?\\d{4}-?\\d{2}";
		Pattern pat = Pattern.compile(regex);
		Matcher mat = pat.matcher (CNPJ);
		
		return mat.matches();
	}
	
	private static int calculationOfFirstVerifierDigit(String CNPJ) {
		int sm, i, num, peso;
		
		sm = 0;
		peso = 2;
		for (i = 11; i >= 0; i--) {
			num = (int) (CNPJ.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso + 1;
			if (peso == 10)
				peso = 2;
		}
		return sm;
	}
	
	private static char checkTheSumOfRest(int r) {
		char dig14;
		if ((r == 0) || (r == 1)) {
			dig14 = '0';
		} else {
			dig14 = (char) ((11 - r) + 48);
		}
		return dig14;
	}
	
	private static int calculationOfSecondVerifierDigit(String CNPJ) {
		int sm, i, num, peso;
		sm = 0;
		peso = 2;
		for (i = 12; i >= 0; i--) {
			num = (int) (CNPJ.charAt(i) - 48);
			sm = sm + (num * peso);
			peso = peso + 1;
			if (peso == 10)
				peso = 2;
		}
		return sm;
	}
	
	private static boolean caracterIsValidInPosition(String CNPJ, char dig13, char dig14) {
		if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) {
			return true;

		} else {
			return false;
		}
	}

}
