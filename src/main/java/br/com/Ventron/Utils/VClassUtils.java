package br.com.Ventron.Utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class VClassUtils {


    /**
     * M�todo usado para validar se h� campos nulos em um objeto.
     * @param object objeto de qualquer classe model.
     * @return Boolean true se possui campo nulo, e false se n�o possuir;
     */
    public static Boolean containsNullField(Object object) {

        for (Method method: object.getClass().getMethods()) {
            if(method.getName().startsWith("get") || method.getName().startsWith("is")) {
                try {
                    Object obj = method.invoke(object, null);
                    if(obj == null) {
                        return true;
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

}
