package br.com.Ventron.Utils;

public class VDataUtils {

    /**
     * Metodo que converte String para Inteiro.
     * Caso valor da String seja invalido, retorna 0.
     * @param s String
     * @return Integer
     */
    public static Integer convertStringToInteger(String s){
        try {
            return Integer.valueOf(s);
        }catch (Exception ex){
            return 0;
        }
    }



    /**
     * Metodo que converte String para Boolean.
     * Caso valor da String seja invalido, retorna false.
     * @param s String
     * @return Boolean
     */
    public static Boolean convertStringToBoolean(String s){
        try {
            return Boolean.valueOf(s);
        }catch (Exception ex){
            return false;
        }
    }


    /**
     * Metodo que converte String para Double.
     * Caso valor da String seja invalido, retorna 0d.
     * @param s String
     * @return Double
     */
    public static Double convertStringToDouble(String s){
        try {
            return Double.valueOf(s);
        }catch (Exception ex){
            return 0D;
        }
    }


    /**
     * Metodo que converte String para Long.
     * Caso valor da String seja invalido, retorna 0.
     * @param s String
     * @return Long
     */
    public static Long convertStringToLong(String s){
        try {
            return Long.valueOf(s);
        }catch (Exception ex){
            return new Long(0);
        }
    }


    /**
     * Metodo que converte String para Byte.
     * Caso valor da String seja invalido, retorna 0.
     * @param s String
     * @return Byte
     */
    public static Byte convertStringToByte(String s){
        try {
            return Byte.valueOf(s);
        }catch (Exception ex){
            return 0;
        }
    }


    /**
     * Metodo que converte String para Short.
     * Caso valor da String seja invalido, retorna 0.
     * @param s String
     * @return Short
     */
    public static Short convertStringToShort(String s){
        try {
            return Short.valueOf(s);
        }catch (Exception ex){
            return 0;
        }
    }

}
