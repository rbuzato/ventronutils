package br.com.Ventron.Utils;

import org.junit.Assert;
import org.junit.Test;

class VCpfTest {

	@Test
	public void testCPFvalido() {
		Assert.assertTrue(VCpfUtils.validarCpf("338.134.838-86"));
	}
	
	@Test
	public void testCPFinvalido() {
		Assert.assertFalse(VCpfUtils.validarCpf("123.987.456-32"));
	}

}
