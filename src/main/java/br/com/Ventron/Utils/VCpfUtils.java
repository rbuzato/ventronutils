package br.com.Ventron.Utils;

public class VCpfUtils {

	private static String documento;

	private static void removerCaracteres() {
		documento = documento.replace("-", "");
		documento = documento.replace(".", "");
	}

	private static boolean validarTamanhoCPF(String cpf) {
		
		if (cpf.length() != 11) {
			return true;
		}
		return false;
	}

	private static boolean validarSequnciaInvalidaCPF(String cpf) {
		if (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222")
				|| cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555")
				|| cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888")
				|| cpf.equals("99999999999")) {
			return true;
		}
		return false;
	}

	private static String calculoDigitoCpf(String cpf) {
		int digGerado = 0;
		int mult = cpf.length() + 1;
		char[] charCpf = cpf.toCharArray();
		for (int i = 0; i < cpf.length(); i++)
			digGerado += (charCpf[i] - 48) * mult--;
		if (digGerado % 11 < 2)
			digGerado = 0;
		else
			digGerado = 11 - digGerado % 11;
		return String.valueOf(digGerado);
	}

	public static boolean validarCpf(String cpf) {

		if (cpf == null) {
			return false;
		} else {
			String cpfGerado = "";
			documento = cpf;

			removerCaracteres();
			if (validarTamanhoCPF(documento)) {
				return false;
			}

			if (validarSequnciaInvalidaCPF(documento)) {
				return false;
			}

			cpfGerado = documento.substring(0, 9);
			cpfGerado = cpfGerado.concat(calculoDigitoCpf(cpfGerado));
			cpfGerado = cpfGerado.concat(calculoDigitoCpf(cpfGerado));

			if (!cpfGerado.equals(documento)) {
				return false;
			}
		}
		return true;
	}
}
