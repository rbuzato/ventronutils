package br.com.Ventron.Utils;


import org.apache.commons.lang3.StringUtils;

public class VStringUtils {

    /**
     * M�todo utilizado para remover caracteres especiais de Strings.
     * @param string String original.
     * @return string sem caracteres especiais.
     */
    public static String removeSpecialCharactersFromString(String string){
          return string.replaceAll("[^a-zA-Z0-9]", "");
    }

}
