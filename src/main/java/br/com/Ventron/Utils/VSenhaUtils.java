package br.com.Ventron.Utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class VSenhaUtils {

	/**
	 * Metodo para criptografar a senha digita em 256 bits
	 * 
	 * @param pwd
	 * @return senha criptografada ou null
	 * @throws NoSuchAlgorithmException
	 */

	public static String criptografarSenhaSha256(String pwd) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");

		if (md != null) {
			return new String(hexCodes(md.digest(pwd.getBytes())));
		}

		return null;
	}

	/**
	 * Metodo para criptografar a senha digita em 512 bits
	 * 
	 * @param pwd
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static String criptografarSenhaSha512(String pwd) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-512");

		if (md != null) {
			return new String(hexCodes(md.digest(pwd.getBytes())));
		}

		return null;
	}

	private static char[] hexCodes(byte[] text) {
		char[] hexOutput = new char[text.length * 2];
		String hexString;

		for (int i = 0; i < text.length; i++) {
			hexString = "00" + Integer.toHexString(text[i]);
			hexString.toUpperCase().getChars(hexString.length() - 2, hexString.length(), hexOutput, i * 2);
		}

		return hexOutput;
	}
}
